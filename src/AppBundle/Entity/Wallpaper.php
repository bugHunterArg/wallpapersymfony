<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Wallpaper
 *
 * @ORM\Table(name="wallpaper")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WallpaperRepository")
 */
class Wallpaper
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=60)
     */
    private $path;

    /**
     * @var string
     * 
     * @Assert\NotBlank(message="Mandatory field.")
     * @Assert\Choice(
     *            choices = {"animals", "cars", "sports", "landscape", "miscellany", "anime", "movies", "abstract", "fantasy", "celebrities", "universe", "games"}, 
     *            message = "Choose a valid category."
     * )
     * @ORM\Column(name="category", type="string", length=50)
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="sizex", type="string", length=15)
     */
    private $sizeX;

    /**
     * @var string
     *
     * @ORM\Column(name="sizey", type="string", length=15)
     */
    private $sizeY;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Tag", mappedBy="wallpapers")
     */
    private $tags;

    /**
     * @var \UserClass
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="uploads")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;

    /**
     * @var int
     *
     * @ORM\Column(name="views", type="integer")
     */
    private $views;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="User", inversedBy="favorites")
     * @ORM\JoinTable(name="users_favorites")
     */
    private $usersFavorite;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="wallpaper")
     */
    private $comments;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->comments = new ArrayCollection();
        $this->usersFavorite = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->views = 100;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Wallpaper
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return Wallpaper
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set size
     *
     * @param string $size
     *
     * @return Wallpaper
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set tags
     *
     * @param array $tags
     *
     * @return Wallpaper
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Get tags
     *
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set user
     *
     * @param \stdClass $user
     *
     * @return Wallpaper
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \stdClass
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set views
     *
     * @param integer $views
     *
     * @return Wallpaper
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return int
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Set usersFavorite
     *
     * @param array $usersFavorite
     *
     * @return Wallpaper
     */
    public function setUsersFavorite($usersFavorite)
    {
        $this->usersFavorite = $usersFavorite;

        return $this;
    }

    /**
     * Get usersFavorite
     *
     * @return array
     */
    public function getUsersFavorite()
    {
        return $this->usersFavorite;
    }

    /**
     * Set comments
     *
     * @param array $comments
     *
     * @return Wallpaper
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return array
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Wallpaper
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add tag
     *
     * @param \AppBundle\Entity\Tag $tag
     *
     * @return Wallpaper
     */
    public function addTag(\AppBundle\Entity\Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag
     *
     * @param \AppBundle\Entity\Tag $tag
     */
    public function removeTag(\AppBundle\Entity\Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Add usersFavorite
     *
     * @param \AppBundle\Entity\User $usersFavorite
     *
     * @return Wallpaper
     */
    public function addUsersFavorite(\AppBundle\Entity\User $usersFavorite)
    {
        $this->usersFavorite[] = $usersFavorite;

        return $this;
    }

    /**
     * Remove usersFavorite
     *
     * @param \AppBundle\Entity\User $usersFavorite
     */
    public function removeUsersFavorite(\AppBundle\Entity\User $usersFavorite)
    {
        $this->usersFavorite->removeElement($usersFavorite);
    }

    /**
     * Add comment
     *
     * @param \AppBundle\Entity\Comment $comment
     *
     * @return Wallpaper
     */
    public function addComment(\AppBundle\Entity\Comment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \AppBundle\Entity\Comment $comment
     */
    public function removeComment(\AppBundle\Entity\Comment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Set sizeX
     *
     * @param string $sizeX
     *
     * @return Wallpaper
     */
    public function setSizeX($sizeX)
    {
        $this->sizeX = $sizeX;

        return $this;
    }

    /**
     * Get sizeX
     *
     * @return string
     */
    public function getSizeX()
    {
        return $this->sizeX;
    }

    /**
     * Set sizeY
     *
     * @param string $sizeY
     *
     * @return Wallpaper
     */
    public function setSizeY($sizeY)
    {
        $this->sizeY = $sizeY;

        return $this;
    }

    /**
     * Get sizeY
     *
     * @return string
     */
    public function getSizeY()
    {
        return $this->sizeY;
    }
}
