<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Tag
 *
 * @ORM\Table(name="tag")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TagRepository")
 * @UniqueEntity("tagName")
 */
class Tag
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Wallpaper", inversedBy="tags")
     * @ORM\JoinTable(name="wall_tag")
     */
    private $wallpapers;

    public function __construct()
    {
        $this->wallpapers = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Tag
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set wallpapers
     *
     * @param array $wallpapers
     *
     * @return Tag
     */
    public function setWallpapers($wallpapers)
    {
        $this->wallpapers = $wallpapers;

        return $this;
    }

    /**
     * Get wallpapers
     *
     * @return array
     */
    public function getWallpapers()
    {
        return $this->wallpapers;
    }

    /**
     * Add wallpaper
     *
     * @param \AppBundle\Entity\Wallpaper $wallpaper
     *
     * @return Tag
     */
    public function addWallpaper(\AppBundle\Entity\Wallpaper $wallpaper)
    {
        $this->wallpapers[] = $wallpaper;

        return $this;
    }

    /**
     * Remove wallpaper
     *
     * @param \AppBundle\Entity\Wallpaper $wallpaper
     */
    public function removeWallpaper(\AppBundle\Entity\Wallpaper $wallpaper)
    {
        $this->wallpapers->removeElement($wallpaper);
    }
}
