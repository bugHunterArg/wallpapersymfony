<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class AdminController extends Controller
{
    /**
     * @Route("/admin/index/", name="admin_index")
     */
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$users = $em->getRepository('AppBundle:User')->findAll();

        return $this->render('admin/admin_index.html.twig', compact('users'));
    }


    /**
     * @Route("/admin/enable/{id}", name="admin_enable")
     */
    public function enableAction(User $user)
    {
    	$em = $this->getDoctrine()->getManager();
    	$user->setEnabled(true);
    	$em->persist($user);
    	$em->flush();

    	$this->addFlash('message', $user->getUsername().' has been enabled!');

    	return $this->redirectToRoute('admin_index');
    }

    /**
     * @Route("/admin/disable/{id}", name="admin_disable")
     */
    public function disableAction(User $user)
    {
    	$em = $this->getDoctrine()->getManager();
    	$user->setEnabled(false);
    	$em->persist($user);
    	$em->flush();

    	$this->addFlash('message', $user->getUsername().' has been disabled!');

    	return $this->redirectToRoute('admin_index');
    }
}
