<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;
use AppBundle\Entity\Wallpaper;
use AppBundle\Entity\Tag;
use AppBundle\Entity\Comment;
use AppBundle\Form\WallpaperType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\Tools\Pagination\Paginator;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig');
    }

    /**
    * @Route("/wallpaper/{id}", name="wallpaper_show")
    */
    public function wallpaperShowAction(Wallpaper $wallpaper)
    {        
        $wallpaper->setViews($wallpaper->getViews()+1);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($wallpaper);
        $em->flush();

        return $this->render('default/wallpaper.html.twig', compact('wallpaper'));
    }

    /**
    * @Route("user/wallpaper/create/", name="wallpaper_create")
    */
    public function wallpaperCreateAction(Request $request)
    {
        $wallpaper = new Wallpaper();
        $form = $this->createForm(WallpaperType::class, $wallpaper,array(
                'method' => 'POST',
                'action' => $this->generateUrl('wallpaper_create')
            ));
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {            
            $imgFile = $form['img']->getData();
            $imgName = md5(uniqid()).'.'.$imgFile->guessExtension();

            $wallpaper->setPath("images/wallpapers/".$imgName);
            $wallpaper->setUser($this->getUser());
            $wallpaper->setCategory($form['category']->getData());
            
            $imgFile->move("images/wallpapers/", $imgName);

            $size = getimagesize($wallpaper->getPath());
            $wallpaper->setSizeX($size[0]);
            $wallpaper->setSizeY($size[1]);
            
            $tags = str_getcsv($form['tag']->getData());
            
            $em = $this->getDoctrine()->getManager();
            $tagRepo = $em->getRepository('AppBundle:Tag');

            foreach($tags as $tagname){
                $tag = $tagRepo->findOneByName($tagname);
                if(empty($tag)){
                    $tag = new Tag();
                    $tag->setName($tagname);
                }
                $tag->addWallpaper($wallpaper);
                $wallpaper->addTag($tag);
                $em->persist($tag);
            }
            $em->persist($wallpaper);
            $em->flush();
            return $this->redirectToRoute("wallpaper_show", array('id'=>$wallpaper->getId()));
        }
        return $this->render("default/wallpaper_create.html.twig", array('form'=>$form->createView()));
    }
    
    /**
    * @Route("/user/comment", name="comment")
    */
    public function commentAction(Request $request)
    {
        $commentContent = filter_var ( $request->get('commentContent'), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $wallpaperid = $request->get('wallpaperid');
        $wallpaper = $this->getDoctrine()->getRepository('AppBundle:Wallpaper')->findOneById($wallpaperid);
        
        $comment = new Comment();
        $comment->setWallpaper($wallpaper);
        $comment->setUser($this->getUser());
        $comment->setContent($commentContent);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($comment);
        $em->flush();

        $commentArray['content'] = $comment->getContent();  
        $commentArray['user'] = $this->getUser()->getUsername();
        $commentArray['created'] = $comment->getCreatedAt()->format('M jS Y \\a\\t g:ia');

        $response = array("comment" => $commentArray);
        return new JsonResponse($response);
    }

    /**
    * @Route("/user/favorite", name="favorite")
    */
    public function favoriteAction(Request $request)
    {
        $wallpaperid = $request->get('wallpaperid');
        $wallpaper = $this->getDoctrine()->getRepository('AppBundle:Wallpaper')->findOneById($wallpaperid);
        
        $user = $this->getUser();
        $user->addFavorite($wallpaper);
        $wallpaper->addUsersFavorite($user);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->persist($wallpaper);
        $em->flush();
        
        return new JsonResponse('ok');
    }

    /**
    * @Route("/wallpapers/category/{name}/{page}", name="wallpaper_category")
    */
    public function categoryAction($name, $page=1)
    {
        $title = $name;
        $query = $this->getDoctrine()->getRepository('AppBundle:Wallpaper')->getCategoryQuery($name);
        $limit = 12;
        $paginator = new Paginator($query, $fetchJoinCollection = true);
        $paginator->getQuery()
        ->setFirstResult($limit * ($page - 1))
        ->setMaxResults($limit);
        $maxPages = ceil($paginator->count() / $limit);
        $wallpapers = $paginator;
        $thisPage = $page;
        
        return $this->render("default/show_wallpapers.html.twig", compact('wallpapers', 'title', 'maxPages', 'thisPage'));
    }

    /**
    * @Route("/wallpapers/tag/{name}/{page}", name="wallpaper_tag")
    */
    public function tagAction($name, $page=1)
    {
        $title = $name;
        $tagid= $this->getDoctrine()->getRepository('AppBundle:Tag')->findOneByName($name)->getId();
        $query = $this->getDoctrine()->getRepository('AppBundle:Wallpaper')->getTagQuery($tagid);
        $limit = 12;
        $paginator = new Paginator($query, $fetchJoinCollection = true);
        $paginator->getQuery()
        ->setFirstResult($limit * ($page - 1))
        ->setMaxResults($limit);
        $maxPages = ceil($paginator->count() / $limit);
        $wallpapers = $paginator;
        $thisPage = $page;
        
        return $this->render("default/show_wallpapers.html.twig", compact('wallpapers', 'title', 'maxPages', 'thisPage'));
    }

    /**
    * @Route("/wallpapers/search/", name="wallpaper_search")
    */
    public function searchAction(Request $request)
    {
        $search = $request->get("search");
        $info = str_getcsv($search, ' ');
        $page = $request->get('page');
        $totaltags = array();
        $repository = $this->getDoctrine()->getRepository('AppBundle:Tag');
        for($f = 0; $f < count($info); $f++){
            $query = $repository->createQueryBuilder('t')
                        ->where('t.name LIKE :tagname')
                        ->setParameter('tagname', '%'.$info[$f].'%')
                        ->orderBy('t.id', 'ASC')
                        ->getQuery();
            $tags = $query->getResult();
            $totaltags = array_merge($totaltags, $tags);
        }
        foreach ($totaltags as $tag) {
            $tagssearch[]=$tag->getId();
        }
        $title = 'Search Result';
        $query = $this->getDoctrine()->getRepository('AppBundle:Wallpaper')->getSearchQuery($tagssearch);
        $limit = 12;
        $paginator = new Paginator($query, $fetchJoinCollection = true);
        $paginator->getQuery()
        ->setFirstResult($limit * ($page - 1))
        ->setMaxResults($limit);
        $maxPages = ceil($paginator->count() / $limit);
        $wallpapers = $paginator;
        $thisPage = $page;

        return $this->render("default/show_wallpapers_search.html.twig", compact('wallpapers', 'title', 'maxPages', 'thisPage', 'search'));
    }

    /**
    * @Route("/user-profile/{id}", name="user_profile")
    */
    public function userProfileAction(User $user)
    {
        return $this->render('FOSUserBundle:Profile:show.html.twig', compact('user'));
    }
}
