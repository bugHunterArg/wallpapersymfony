<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints as Assert;

class WallpaperType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('category', ChoiceType::class, array(
                        'choices' => array(
                                    'Abstract' => 'abstract',
                                    'Animals' => 'animals',
                                    'Anime' => 'anime',
                                    'Cars' => 'cars',
                                    'Celebrities' => 'celebrities',
                                    'Fantasy' => 'fantasy',
                                    'Games' => 'games',
                                    'Landscape' => 'landscape',
                                    'Miscellany' => 'miscellany',
                                    'Movies' => 'movies',
                                    'Sports' => 'sports',
                                    'Universe' => 'universe',
                    )))
                ->add('img', FileType::class, array(
                    'mapped'=>false,
                    'constraints' => array(
                        new Assert\NotBlank(),
                        new Assert\Image(array(
                                    'maxSize' => "4096k",
                                    'mimeTypes' =>"image/jpeg",
                                    'mimeTypesMessage' => "Please upload a valid JPEG/JPG image file."
                                ))
                        )
                    ))
                ->add('tag', TextType::class, array(
                    'mapped'=>false,
                    'constraints' => array(
                        new Assert\NotBlank(),
                        new Assert\Type(array('type' => "string")),
                        new Assert\Length(array("max" => 250))
                        )
                ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Wallpaper'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_wallpaper';
    }


}
